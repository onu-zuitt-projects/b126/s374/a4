let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDescription");
let coursePrice = document.querySelector("#coursePrice");

let token = localStorage.getItem("token")

 fetch(`http://localhost:4000/courses/${courseId}`)
 .then(res => res.json())
 .then(data => {
 	console.log(data)
 	courseName.value = data.name;
 	courseDesc.value = data.description;
 	coursePrice.value = data.price; 

    document.querySelector("#editCourse").addEventListener("submit", (e) => {
        e.preventDefault

        //variables representing the values of our inputs 
        let name = courseName.value
        let desc = courseDesc.value 
        let price = coursePrice.value 

        /*
            ACTIVITY: Create a PUT request via fetch that will send our new, updated course information to the server and handle the server's response. If the server responds with true (use === true), show an alert that confirms successful course updating, then redirect the user to courses.html.

            If not, show an alert with an error message.
        */

    fetch(`http://localhost:4000/courses/${courseId}`), {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                        Authorization: `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price
                    })
                    .then(res => res.json())
                    .then(data => {
                

                    if(data === true){
                        alert("Successfully updated.")
                        //after successful updating, redirect user to the courses page.
                        window.location.replace("./courses.html")
                    }else{
                        alert("Error. Update failed.")
                    }
                
                }

            }else{
            alert("Please check your details and try again.")
    

        }

    })	
})
